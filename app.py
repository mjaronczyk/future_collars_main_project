from flask import Flask
from flask import Blueprint
from modules.shared import db



app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///modules/mydatabase.db'
db.init_app(app)
app.secret_key = "dsadsad32r32r2rdafsfds8fds6fd7sfsdcds7csd7cdsc78sdcs7c8s7c872e2c"

from modules.paths import paths

app.register_blueprint(paths)


app.run(debug = True)






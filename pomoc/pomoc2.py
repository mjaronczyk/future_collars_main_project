from website.modules.db import User




class Betting:

    def __init__(self, login):
        self.login = login
        self.types = {}
        self.matches = {}
        self.load_data()


    
    def load_data(self):
        with open('engine/matches.txt') as file:
            for line in file.readlines():
                id, date, time, first, second = line.split()
                self.matches[id] = {
                    "date":date,
                    "time": time,
                    "first_team": first,
                    "second_team": second,
                    }
        

    

    def add_bet(self, id, winner):
        if id in self.matches and winner in self.matches[id].values():
            self.types[id] = winner
    

    def delete_bet(self, id):
        if id in self.types:
            del self.types[id]

            
            
            





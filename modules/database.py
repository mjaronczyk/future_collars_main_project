from flask_sqlalchemy import SQLAlchemy
from modules.shared import db
import random




class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(120), unique=False, nullable=False)
    user_login = db.Column(db.String(80), unique=True, nullable=False)
    password = db.Column(db.String(120), unique=False, nullable=False)
    credits = db.Column(db.Integer, unique=False, nullable=True)
    points = db.Column(db.Integer, unique=False, nullable=True)




class Matches(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    match_id = db.Column(db.Integer, unique=True, nullable=False)
    date = db.Column(db.String(80), unique=False, nullable=False)
    first_team = db.Column(db.String(120), unique=False, nullable=False)
    second_team = db.Column(db.String(120), unique=False, nullable=False)
    won = db.Column(db.String(120), unique=False, nullable=True)


class Bets(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    bet_id = db.Column(db.Integer, unique=True, nullable=False)
    user_login = db.Column(db.String(80), unique=False, nullable=False)
    match_id = db.Column(db.Integer, unique=False, nullable=False)
    winner_bet = db.Column(db.String(120), unique=False, nullable=False)
    


# with open('C:/programy/visual studio code/2 tura/flask/project_zaklady/website/modules/matches.txt') as file:
#     for element in file.readlines():
#         id = random.randrange(1,9999)
#         date, first, second = element.split()
#         print(id, date, first, second)
#         match = Matches(match_id=id, date=date, first_team=first, second_team=second)
#         db.session.add(match)
#         db.session.commit()


# user_db = User(name='name', user_login='log', password='password1', credits=100, points=0)
# db.session.add(user_db)
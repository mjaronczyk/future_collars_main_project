from flask import Blueprint, render_template, request, session
# from modules.db import User
from modules.database import User, Matches, Bets
from modules.shared import db
from modules.engine import Teams
import random



paths = Blueprint('paths', __name__)


# def save_login(login):
#     with open("modules/self.login.txt", "a") as file:
#         if login:
#             file.write(f'{login}\n')

# def load_login():
#     with open("modules/self.login.txt") as file:
#         return file.readline().strip()

# def delete_login():
#     with open("modules/self.login.txt", "w") as file:
#         file.write("")
        


@paths.route('/')
def home():
    return render_template("home.html")


# @paths.route('/delete_table')
# def delete_table():
#     for i in range(10):
#         Bets.query.filter_by(id=i).delete()
#         db.session.commit()
#     return "ANULOWANO!"


# @paths.route('/create_base')
# def create_table():
#     db.create_all()
#     return "Zrobiono!"
   

# @paths.route('/add_matches')
# def add_matches():
#     Teams().update_matches_list()
#     return "dodano!"

# @paths.route('/add_bet')
# def add_bet():
#     Teams().add_bet(bet_id=34233, user_login='aaa', match_id=9793, winner_bet='Polska')
#     return "dodano!"



# @paths.route('/add_winner')
# def add_winner():
#     Teams().add_winner(9673, 'Niemcy')
#     return "dodano!"

# @paths.route('/add_any')
# def add_any():
#     user = User.query.filter_by(user_login = "Julia").first()
#     user.points = 2
#     db.session.commit()
#     return "dodano!"


@paths.route('/login', methods = ['GET', 'POST'])
def login():
    is_succes = False
    name = request.form.get("name")
    log = request.form.get("login")
    password1 = request.form.get("password1")
    password2 = request.form.get("password2")
    
    if password1 == password2 and name and log:
        try:
            user_db = User(name=name, user_login=log, password=password1, credits=100, points=0)
            db.session.add(user_db)
            db.session.commit()
            message = "Zarejestrowano"
            is_success = True
            print(is_success)
            return render_template("login.html", message=message, is_success=is_success)
        except:
            message = "Błąd formularza. Zajety Login"
            return render_template("register.html", message=message)
    elif password1 != password2:
        message = "Błąd! Hasła się różnią"
        return render_template("register.html", message=message)
        
    return render_template("login.html")
    

    


@paths.route('/register', methods = ['GET', 'POST'])
def register():
    return render_template("register.html")



@paths.route('/profile', methods = ['GET', 'POST'])
def profile():
    log = request.form.get("login")
    password = request.form.get("password")
    if log and password:
        user = User.query.filter_by(user_login=log).first()
        if log == user.user_login and password == user.password:
            session['user'] = log
        else:
            message = "Niepoprawny login lub hasło"
            
    matches = Teams().show_teams()

    if request.form:
        try:
            bet_request = request.form.get('key').split()
            print(bet_request)
            bet_id = id = random.randrange(1,999999)
            match_id = int(bet_request[0])
            winner = bet_request[1]
            Teams().add_bet(bet_id=bet_id, user_login=session['user'], match_id=match_id, winner_bet=winner)
        except:
            pass
            
        
    if session:
        return render_template("profile.html", login=session['user'], matches=matches)
    else:
        return render_template("home.html")





@paths.route('/ranking')
def ranking():
    logged = False
    users = Teams().show_all_users_rank()
    if session:
        logged = True
        return render_template("ranking.html",  
                                logged=logged, 
                                login=session['user'],
                                users = users)
    else:
        return render_template("ranking.html", users = users)



@paths.route('/my_bets', methods = ['GET', 'POST'])
def my_bets():
    edition = False
    bet_id = request.form.get("option1")
    match_id = request.form.get("option2")
    winner = request.form.get("option3")
    
    if request.form.get("action") == "Delete":
        Teams().delete_bet(bet_id, session['user'])
        pass
    elif request.form.get("action") == "Edit":
        edition = True
        
        
    if request.form.get('action_edit'):
        winner = request.form.get('key_edit')
        bet_id = request.form.get('bet_id')
        Teams().edit_bet(bet_id, winner)

    if session:
        engine_teams = Teams()
        first_teams = engine_teams.teams_to_edit(session['user'])[0]
        second_teams = engine_teams.teams_to_edit(session['user'])[1]
        print(first_teams, second_teams)
        
        bets_db = Teams().show_valid_bets(session['user'])

        return render_template("bets.html", 
                login=session['user'], 
                bets_db=bets_db, 
                edition=edition, 
                first=first_teams, 
                second=second_teams)

    else:
        return render_template("login.html")


@paths.route('/log_out')
def log_out():
    session.pop('user', None)
    message = "Wylogowano"
    return render_template("home.html", message=message)


@paths.route('/dashboard')
def dashboard():
    if session:
        user = Teams().get_user_info(session['user'])
        bets, won, lost = Teams().show_all_bets(session['user'])
        return render_template("dashboard.html", user=user, login=session['user'], bets=bets, won=won, lost=lost)
    else:
        message = "Musisz być zalogowany"
        return render_template("login.html", message=message)
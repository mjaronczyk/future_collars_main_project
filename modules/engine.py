from modules.database import User, Matches, Bets
from modules.shared import db
from datetime import datetime
import random


class Teams:
    
    def __init__(self):
        self.present = datetime.now()


    def show_valid_bets(self, user_login):
        maches_id = []
        bets_in_progress = []
        all = Matches.query.all()
        for element in all:
            if element.won:
                maches_id.append(element.match_id)
        
        bets = Bets.query.filter_by(user_login=user_login).all()
        for bet in bets:
            if bet.match_id not in maches_id:
                bets_in_progress.append(bet)
        
        return bets_in_progress

        

    def add_bet(self, bet_id, user_login, match_id, winner_bet):
        user = User.query.filter_by(user_login=user_login).first()
        if user.credits >= 10:
            bet_db = Bets(bet_id=bet_id, user_login=user_login, match_id=match_id, winner_bet=winner_bet)
            db.session.add(bet_db)
            user.credits += -10
            db.session.commit()
        else:
            print(" TU BĘDZIE FLASHING MESSAGE! NIE MASZ WYSTARCZAJĄCO CREDITOW! DOLADUJ KREDITY!")
            

    def delete_bet(self, bet_id, user_login):
        user = User.query.filter_by(user_login=user_login).first()
        Bets.query.filter_by(id=bet_id).delete()
        user.credits += 10
        db.session.commit()
    

    def edit_bet(self, bet_id, new_winner):
        bet = Bets.query.filter_by(bet_id=bet_id).first()
        date = Matches.query.filter_by(match_id = bet.match_id).first()
        match_time = datetime.strptime(date.date, "%d.%m.%Y")
        if match_time.date() > self.present.date():
            bet.winner_bet = new_winner
            db.session.commit()
    
    def show_all_bets(self, user_login):
        win_bets = []
        lost_bets = []
        bets = Bets.query.filter_by(user_login=user_login).all()
        matches = Matches.query.all()
        for match in matches:
            for bet in bets:
                if match.match_id == bet.match_id: 
                    if match.won == bet.winner_bet:
                        win_bets.append(bet)
                    elif match.won != bet.winner_bet and match.won:
                        lost_bets.append(bet)


        return bets, win_bets, lost_bets
    

    def show_teams(self):
        matches_var = []
        matches = Matches.query.all()
        for match in matches:
            if datetime.strptime(match.date, "%d.%m.%Y").date() > self.present.date():
                matches_var.append(match)
        return matches_var
        # match_time = datetime.strptime(matches.date, "%d.%m.%Y")
        # if match_time.date() > self.present.date():
        #     print("tak!")



    def teams_to_edit(self, user_login):
        first_teams = []
        second_teams = []
        id = []

        for element in self.show_valid_bets(user_login):
            id.append(element.match_id)

        for i in id:
            first_teams.append(Matches.query.filter_by(match_id=i).first().first_team)
            second_teams.append(Matches.query.filter_by(match_id=i).first().second_team)
        return first_teams, second_teams
    

    def update_matches_list(self):
        with open('modules/matches.txt') as file:
            for element in file.readlines():
                id = random.randrange(1,99999)
                date = element.split()[0]
                first = element.split()[1]
                second = element.split()[2]
                print(id, date, first, second)
                matches = Matches(match_id=id, date=date, first_team=first, second_team=second, won=None)
                db.session.add(matches)
        db.session.commit()
    

    def get_user_info(self, user_login):
        user = User.query.filter_by(user_login = user_login).first()
        return user
    

    def show_all_users_rank(self):
        users = db.session.query(User).order_by(User.points.desc())
        return users
    

    def add_winner(self, match_id, winner):
        match = Matches.query.filter_by(match_id = match_id).first()
        match.won = winner
        if match.first_team == winner or match.second_team == winner:
            bets = Bets.query.filter_by(match_id=match.match_id).all()
            for bet in bets:
                if bet.winner_bet == match.won:
                    user = User.query.filter_by(user_login=bet.user_login).first()
                    user.points += 1
                    user.credits += 10
        db.session.commit()


        # DODAJ TU PRZYZNAWANIE PUNKTOW ODRAZU DLA TYCH CO WYGRALI ZAKLAD! I POMYSL CZY NEI COS JESZCZE